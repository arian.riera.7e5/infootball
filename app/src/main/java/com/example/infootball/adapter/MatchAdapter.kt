package com.example.infootball.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.infootball.MatchGeneral
import com.example.infootball.OnClickListener
import com.example.infootball.R
import com.example.infootball.databinding.MatchTargetBinding
import com.squareup.picasso.Picasso

class MatchAdapter(private val matches: List<MatchGeneral>, private val listener: OnClickListener): RecyclerView.Adapter<MatchAdapter.ViewHolder>() {

    private lateinit var context: Context

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        context = parent.context
        val view = LayoutInflater.from(context).inflate(R.layout.match_target, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val match = matches[position]
        with(holder){
            setListener(match)
            binding.title.text = match.title
            binding.league.text = match.competition
            Picasso.get()
                .load(match.thumbnail)
                .into(binding.image)
            binding.rounded.radius = 40f
        }
    }

    override fun getItemCount(): Int {
        return matches.size
    }

    inner class ViewHolder(view: View): RecyclerView.ViewHolder(view){
        val binding = MatchTargetBinding.bind(view)

        fun setListener(match: MatchGeneral){
            binding.root.setOnClickListener {
                listener.onClick(match)
            }
        }
    }
}