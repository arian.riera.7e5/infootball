package com.example.infootball.room

import androidx.room.*

@Dao
interface MatchDao {

    @Query("SELECT * FROM MatchEntity")
    fun getAllMatches(): MutableList<MatchEntity>

    @Insert
    fun addMatch(matchEntity: MatchEntity)

    @Query("SELECT COUNT(*) FROM MatchEntity WHERE title = :title")
    fun isFavorite(title: String): Int

    @Query("DELETE FROM MatchEntity WHERE title = :title")
    fun deleteMatch(title: String)


}