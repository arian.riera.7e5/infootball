package com.example.infootball.room

import androidx.room.Database
import androidx.room.RoomDatabase

@Database(entities = [MatchEntity::class], version = 2)
abstract class MatchDatabase: RoomDatabase() {
    abstract fun matchDao(): MatchDao
}