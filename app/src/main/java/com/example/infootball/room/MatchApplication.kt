package com.example.infootball.room

import android.app.Application
import androidx.room.Room

class MatchApplication: Application() {
    companion object {
        lateinit var database: MatchDatabase
    }

    override fun onCreate() {
        super.onCreate()
        database = Room.databaseBuilder(this,
            MatchDatabase::class.java,
            "MatchDatabase").build()
    }
}