package com.example.infootball.room

import android.os.Parcelable
import androidx.room.Entity
import androidx.room.PrimaryKey
import kotlinx.parcelize.Parcelize

@Parcelize
@Entity(tableName = "MatchEntity")
data class MatchEntity(
    @PrimaryKey(autoGenerate = true) var id: Long = 0,
    var competition: String,
    var competitionUrl: String,
    var date: String,
    var matchviewUrl: String,
    var thumbnail: String,
    var title: String,
    var embed: String,
    var embedTitle: String) : Parcelable