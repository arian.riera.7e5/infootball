package com.example.infootball.fragments

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.Navigation
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.infootball.MatchGeneral
import com.example.infootball.OnClickListener
import com.example.infootball.R
import com.example.infootball.adapter.MatchAdapter
import com.example.infootball.databinding.FragmentRecyclerViewBinding
import com.example.infootball.model.MatchViewModel

class AllMatchesFragment : Fragment(), OnClickListener{

    private lateinit var matchAdapter: MatchAdapter
    private lateinit var linearLayoutManager: RecyclerView.LayoutManager
    private lateinit var binding: FragmentRecyclerViewBinding
    private val model: MatchViewModel by activityViewModels()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        binding = FragmentRecyclerViewBinding.inflate(layoutInflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        model.fetchData()

        model.data.observe(viewLifecycleOwner, {
            Log.e("CANVIS AL VIEWMODEL", model.data.value.toString())
            setUpRecyclerView(it)
        })

    }


    private fun setUpRecyclerView(myData: List<MatchGeneral>) {
        matchAdapter = MatchAdapter(myData, this)
        linearLayoutManager = LinearLayoutManager(context)

        binding.recyclerView.apply {
            setHasFixedSize(true) //Optimitza el rendiment de l’app
            layoutManager = linearLayoutManager
            adapter = matchAdapter
        }
    }

    override fun onClick(match: MatchGeneral) {
        model.setMatch(match)
        view?.let { Navigation.findNavController(it).navigate(R.id.action_allMatchesFragment_to_detailMatchFragment) }
    }

}