package com.example.infootball.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.webkit.WebChromeClient
import android.widget.FrameLayout
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.constraintlayout.widget.Guideline
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentContainerView
import androidx.fragment.app.activityViewModels
import com.example.infootball.R
import com.example.infootball.databinding.FragmentMatchDetailsBinding
import com.example.infootball.jsontokotlin.Video
import com.example.infootball.model.MatchViewModel
import com.example.infootball.room.MatchApplication
import com.example.infootball.room.MatchEntity
import com.google.android.material.bottomnavigation.BottomNavigationView
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch


class DetailMatchFragment : Fragment() {


    private val model: MatchViewModel by activityViewModels()
    lateinit var fullscreenView: View
    lateinit var binding: FragmentMatchDetailsBinding

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View {
        binding = FragmentMatchDetailsBinding.inflate(layoutInflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        model.actualMatch.observe(viewLifecycleOwner, {
            val match = it
            binding.title.text = match.title
            binding.league.text = match.competition
            val dateToChar = match.date.toCharArray()
            val dateCorrect: String = dateToChar[8].toString()+dateToChar[9].toString()+dateToChar[7].toString()+dateToChar[5].toString()+dateToChar[6].toString()+dateToChar[4].toString()+dateToChar[0].toString()+dateToChar[1].toString()+dateToChar[2].toString()+dateToChar[3].toString()+" | "+dateToChar[11].toString()+dateToChar[12].toString()+dateToChar[13].toString()+dateToChar[14].toString()+dateToChar[15].toString()+dateToChar[16].toString()+dateToChar[17].toString()+dateToChar[18].toString()
            binding.date.text = dateCorrect
            val videos: List<Video> = match.videos
            binding.videoType.text = videos[0].title
            val linkVideo = videos[0].embed
            val directLink = linkVideo.split("'")
            loadWebpage(directLink[3])

            CoroutineScope(Dispatchers.IO).launch {
                binding.cbFavourite.isChecked = MatchApplication.database.matchDao().isFavorite(it.title) != 0
            }
        })

        binding.cbFavourite.setOnClickListener {
            val match = model.actualMatch.value!!
            val video = match.videos[0]
            val matchEdit = MatchEntity(0, match.competition, match.matchviewUrl, match.date, match.matchviewUrl, match.thumbnail, match.title, video.embed, video.title)
            CoroutineScope(Dispatchers.IO).launch {
                if (MatchApplication.database.matchDao().isFavorite(match.title) == 0) {
                    MatchApplication.database.matchDao().addMatch(matchEdit)
                } else {
                    MatchApplication.database.matchDao().deleteMatch(matchEdit.title)
                }
            }

        }
    }

    private fun loadWebpage(link: String) {
        binding.webview.settings.javaScriptEnabled = true
        binding.webview.settings.domStorageEnabled = true
        fullScreen()
        try {
            binding.webview.loadUrl(link)
        } catch(e: UnsupportedOperationException) {
            e.printStackTrace()
        }
    }

    private fun fullScreen() {

        binding.webview.webChromeClient = object: WebChromeClient() {

            override fun onShowCustomView(view: View?, callback: CustomViewCallback?) {
                super.onShowCustomView(view, callback)

                if (view is FrameLayout) {
                    fullscreenView = view
                    binding.fullscreenContainer.addView(fullscreenView)
                    binding.fullscreenContainer.visibility = View.VISIBLE
                    binding.viewContainer?.visibility  = View.GONE

                    val navBar: BottomNavigationView = activity!!.findViewById(R.id.bottom_navigation)
                    navBar.visibility = View.GONE
                    val container: FragmentContainerView = activity!!.findViewById(R.id.fragmentContainerView)
                    val params = container.layoutParams as ConstraintLayout.LayoutParams
                    params.bottomToBottom = R.id.complete_box

                }

            }

            override fun onHideCustomView() {
                super.onHideCustomView()

                binding.fullscreenContainer.removeView(fullscreenView)
                binding.fullscreenContainer.visibility = View.GONE
                binding.viewContainer?.visibility = View.VISIBLE

                val navBar: BottomNavigationView = activity!!.findViewById(R.id.bottom_navigation)
                navBar.visibility = View.VISIBLE
                val container: FragmentContainerView = activity!!.findViewById(R.id.fragmentContainerView)
                val params = container.layoutParams as ConstraintLayout.LayoutParams
                val guide: Guideline = activity!!.findViewById(R.id.guideline)
                params.bottomToBottom = guide.id

            }

        }
    }

}


