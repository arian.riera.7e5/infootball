package com.example.infootball.jsontokotlin

import com.google.gson.annotations.SerializedName

data class Data (
    @SerializedName("response")
    val matches: List<Match>)