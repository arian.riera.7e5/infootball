package com.example.infootball.jsontokotlin

data class Video(
    val embed: String,
    val title: String
)