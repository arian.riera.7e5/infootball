package com.example.infootball

import com.example.infootball.jsontokotlin.Data
import com.example.infootball.jsontokotlin.Match

data class DataGeneral(
    val matches: List<Match>
)

fun Data.toDataGeneral() = DataGeneral(matches)