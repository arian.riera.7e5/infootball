package com.example.infootball

import com.example.infootball.jsontokotlin.Match
import com.example.infootball.jsontokotlin.Video
import com.example.infootball.room.MatchEntity

data class MatchGeneral(
    val competition: String,
    val competitionUrl: String,
    val date: String,
    val matchviewUrl: String,
    val thumbnail: String,
    val title: String,
    val videos: List<Video>
    )

fun generateListVideo(embed: String, title: String): List<Video> {
    val video = Video(embed, title)
    return listOf(video)
}

fun Match.toMatchGeneral() = MatchGeneral(competition, competitionUrl, date, matchviewUrl, thumbnail, title, videos)
fun MatchEntity.toMatchGeneral() = MatchGeneral(competition, competitionUrl, date, matchviewUrl, thumbnail, title, generateListVideo(embed, embedTitle))