package com.example.infootball.model

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.infootball.MatchGeneral
import com.example.infootball.Repository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class MatchViewModel: ViewModel(){

    private val repository = Repository()
    var data = MutableLiveData<List<MatchGeneral>>()
    var dataRoom = MutableLiveData<List<MatchGeneral>>()
    var dataRoomEmpty = MutableLiveData<Boolean>()
    var actualMatch = MutableLiveData<MatchGeneral>()
    private val endpoint = "v3"

    init {
        fetchData()
    }

    fun fetchData(){
        viewModelScope.launch {
            val response = withContext(Dispatchers.IO){repository.fetchMatchesAPI(endpoint)}
            data.value = response
        }
    }

    fun fetchDataRoom(){
        viewModelScope.launch {
            val response = withContext(Dispatchers.IO){repository.fetchMatchesRoom()}
            if (response[0].title!="") {
                dataRoomEmpty.value = false
                dataRoom.value = response
            } else {
                Log.e("CHIVATO", "ESTA VACIA")
                dataRoomEmpty.value = true
            }
        }
    }

    fun setMatch(match: MatchGeneral){
        actualMatch.value = match
    }

}