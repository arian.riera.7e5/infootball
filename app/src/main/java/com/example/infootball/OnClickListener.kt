package com.example.infootball

interface OnClickListener {
    fun onClick(match: MatchGeneral)
}