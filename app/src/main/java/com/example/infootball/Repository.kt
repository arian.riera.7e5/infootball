package com.example.infootball

import android.util.Log
import com.example.infootball.api.ApiInterface
import com.example.infootball.room.MatchApplication
import com.example.infootball.room.MatchEntity

class Repository {

    private val apiInterface = ApiInterface.create()

    suspend fun fetchMatchesAPI(url: String): List<MatchGeneral> {
        val response = apiInterface.getAllMatches(url)
        lateinit var results: DataGeneral
        if(response.isSuccessful){
            results = response.body()!!.toDataGeneral()
        }
        else{
            Log.e("ERROR", "L'API NO HA DONAT UNA RESPOSTA VALIDA")
        }
        return results.matches.map { it.toMatchGeneral() }
    }

    fun fetchMatchesRoom(): List<MatchGeneral> {
        val response = MatchApplication.database.matchDao().getAllMatches()
        lateinit var results: List<MatchEntity>
        if(response.isEmpty()){
            Log.e("ERROR", "LA BASE DE DADES ESTA BUIDA")
            val match = MatchEntity(0, "", "", "", "", "https://www.scorebat.com/og/m/og1123160.jpeg", "", "", "")
            val list: List<MatchEntity> = listOf(match)
            results = list
        } else {
            results = response
        }
        return results.map { it.toMatchGeneral() }
    }

}